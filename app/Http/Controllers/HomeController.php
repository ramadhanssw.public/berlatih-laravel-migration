<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('data.form');
    }

    public function login(Request $request)
    {
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        return view('data.index', compact('firstName', 'lastName'));
    }
}
